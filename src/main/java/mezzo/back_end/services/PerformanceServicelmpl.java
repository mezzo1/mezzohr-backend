package mezzo.back_end.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mezzo.back_end.Repositories.ChallengeRepository;
import mezzo.back_end.Repositories.CongeRepository;
import mezzo.back_end.Repositories.PerformanceRepository;
import mezzo.back_end.Repositories.UserRepository;
import mezzo.back_end.Response.CongeResponse;
import mezzo.back_end.entities.Challenge;
import mezzo.back_end.entities.Conge;
import mezzo.back_end.entities.Performance;
import mezzo.back_end.entities.User;

@Service
public class PerformanceServicelmpl implements PerformanceService  {
	

	@Autowired
	private PerformanceRepository  pr;
	@Autowired
	private UserRepository ur;
	@Autowired
	CongeRepository cr;
	@Autowired
	ChallengeRepository chr;
	@Override
	public List<Performance> findByUserId(Long id) {
		User user = ur.findById(id).get();
		return pr.findByUser(user);
		
	}


	@Override
	public Performance createPerformancee(Conge conge) {
		long nbrTh=0;
		long nbrTj=0;
		User user = ur.findById(conge.getUser().getId()).get();
		Performance p = new Performance();
		p.setUser(user);
				Calendar cal= Calendar.getInstance(); 
				cal.setTime(conge.getDate_debut());
				int yearAb=cal.get(Calendar.YEAR);
				
				
				
				 long time_difference = conge.getDate_fin().getTime() - conge.getDate_debut().getTime();
				 long nbrJ=time_difference / (1000*60*60*24);
				 long nbrHH=nbrJ*24;
				 nbrTh=nbrTh+nbrHH;	
				 nbrTj=nbrTj+nbrJ; 
				
			    p.setNbrHHC(nbrTh);
				p.setNbrDayC(nbrTj);
				p.setPerformanceYear(yearAb);
				float absences=(nbrTh*100)/1680;
				p.setTauxAbsences(absences);
				return pr.save(p);
		}



	
	
	@Override
	public Performance getPerformanceById(Long Id) {
Optional<Performance> opt = pr.findById(Id);
		
		if(opt.isEmpty())
			return null;
		else
			return  opt.get();
	}

	@Override
	public Performance getUserPerformanceByYear(Long Id,int year) {
		User user = ur.findById(Id).get();
		List<Performance> Performances =pr.findByUser(user);
		if (Performances.isEmpty()) {
			return null;
		}
		else {for (Performance performance : Performances) {
			if(performance.getPerformanceYear()==year) {
				
				
				return performance;
			}
			}
		}
		
		return null;		
}



	@Override
	public Performance updatePerformance(Long id,Conge conge) {
		long nbrTh=0;
		long nbrTj=0;
		Performance p =pr.findById(id).get();
		 long time_difference = conge.getDate_fin().getTime() - conge.getDate_debut().getTime();
		 long nbrJ=time_difference / (1000*60*60*24);
		 long nbrHH=nbrJ*24;
		 nbrTh=p.getNbrHHC()+nbrHH;	
		 nbrTj=p.getNbrDayC()+nbrJ; 
	    p.setNbrHHC(nbrTh);
		p.setNbrDayC(nbrTj);
		float absences=(nbrTh*100)/1680;
		p.setTauxAbsences(absences);
		return pr.save(p);
	}
}
	









