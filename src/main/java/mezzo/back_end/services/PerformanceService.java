package mezzo.back_end.services;

import java.util.List;

import mezzo.back_end.entities.Challenge;
import mezzo.back_end.entities.Conge;
import mezzo.back_end.entities.Performance;
import mezzo.back_end.entities.User;

public interface PerformanceService {

	public List<Performance> findByUserId(Long id);
	public Performance getPerformanceById(Long Id);
	public Performance createPerformancee(Conge conge);
	public Performance getUserPerformanceByYear(Long Id, int year);
	//public Performance UpdatePerformance(Performance p,Long id, int year);
	public Performance  updatePerformance(Long id,Conge c);
	


}

