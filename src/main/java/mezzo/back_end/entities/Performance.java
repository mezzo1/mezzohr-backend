package mezzo.back_end.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class Performance implements Serializable{
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		private long nbrDayC;
		private long nbrHHC;
		private long performanceYear;
		private float TauxAbsences;
		@ManyToOne(fetch = FetchType.LAZY, optional = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private User user;
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public long getNbrDayC() {
			return nbrDayC;
		}
		public void setNbrDayC(long nbrDayC) {
			this.nbrDayC = nbrDayC;
		}
		public long getNbrHHC() {
			return nbrHHC;
		}
		public void setNbrHHC(long nbrHHC) {
			this.nbrHHC = nbrHHC;
		}
	
		public float getTauxAbsences() {
			return TauxAbsences;
		}
		public void setTauxAbsences(float tauxAbsences) {
			TauxAbsences = tauxAbsences;
		}
	
		public long getPerformanceYear() {
			return performanceYear;
		}
		public void setPerformanceYear(long performanceYear) {
			this.performanceYear = performanceYear;
		}
	}
