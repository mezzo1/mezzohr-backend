package mezzo.back_end.Controllers;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mezzo.back_end.Request.CongeRequest;
import mezzo.back_end.Response.CongeResponse;
import mezzo.back_end.Response.PerformanceResponse;
import mezzo.back_end.entities.Challenge;
import mezzo.back_end.entities.Conge;
import mezzo.back_end.entities.Performance;
import mezzo.back_end.entities.User;
import mezzo.back_end.services.CongeService;
import mezzo.back_end.services.PerformanceService;
import mezzo.back_end.services.UserService;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/Performance")
public class PerformanceController {
	@Autowired
	private PerformanceService ps;	

//methode pou recuperer tous les performances d un employe 
	@GetMapping("/{userId}")
	  public List<PerformanceResponse> getPerformanceByUserId(@PathVariable(value = "userId") Long userId) {

		List<Performance> performancee = ps.findByUserId(userId);
		Type listType = new TypeToken<List<PerformanceResponse>>() {}.getType();

		List<PerformanceResponse> PerformanceResponse = new ModelMapper().map(performancee, listType);
		
		return  PerformanceResponse;
	  }



	
	
//methode pour recuperer une perfermance d un employe par annee
	@GetMapping("/{userId}/{year}")
	public Performance getUserPerformanceByYear(@PathVariable(value = "userId") Long Id,@PathVariable int year) {
		List<Performance> Performances =ps.findByUserId(Id);
		if (Performances.isEmpty()) {
			return null;
		}
		else {for (Performance performance : Performances) {
			if(performance.getPerformanceYear()==year) {
				
				
				return performance;
			}
			}
		}
		return null;
	}
}
	



